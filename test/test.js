const _ = require('lodash');
const fs = require('fs');

let rawdata = fs.readFileSync('../public/tree.json');
let tree = JSON.parse(rawdata);


let stream = tree.streams[0];

let sources =  getUniqueLocations(stream, 'source');
let targets =  getUniqueLocations(stream, 'destination');
console.info(sources);
console.info("");
console.info(targets);
console.info("");


//_.forEach([1,2,3], (it) => console.info(it));

function getUniqueLocations(stream, property) {
    let res =  _.flatMap(stream.paths, p => p[property].networkLocation);
    res = _.uniqBy(res, it => it.switchId + it.port);
    res = _.orderBy(res, ['switchId', 'port']);
    return res;
}

